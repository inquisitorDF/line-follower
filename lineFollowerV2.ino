#include <avr/pgmspace.h>
#include <Wire.h>
#include <Zumo32U4.h>

//<----------------Setup Zumo----------------->
Zumo32U4ButtonA buttonA;
Zumo32U4Motors motors;
Zumo32U4LineSensors sensors;
//<---------------------------------------------->

unsigned int sensor_values[3];
uint8_t mSpeed = 400;
int line = 600;
uint8_t x = 10;

void setup() {
  sensors.calibrate();
  sensors.initThreeSensors();
  buttonA.waitForButton();
}

void loop() {
  sensors.read(sensor_values);

  if(line < sensor_values[1]) {
    motors.setSpeeds(mSpeed, mSpeed);
  } 
  else if(line < sensor_values[0] && line > sensor_values[1]) {
    motors.setSpeeds(mSpeed/x, mSpeed);
  }
  else if(line < sensor_values[2] && line > sensor_values[1]) {
    motors.setSpeeds(mSpeed, mSpeed/x);
  }
  else if(line > sensor_values[0] && line > sensor_values[1] && line > sensor_values[2]) {
    motors.setSpeeds(mSpeed/x, mSpeed); 
  }
  
}
