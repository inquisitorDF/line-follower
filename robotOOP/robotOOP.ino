#include <avr/pgmspace.h>
#include <Wire.h>
#include <Zumo32U4.h>
//<----------------Setup Zumo----------------->
Zumo32U4Buzzer buzzer;
Zumo32U4ButtonA buttonA;
Zumo32U4ButtonB buttonB;
Zumo32U4ButtonC buttonC;
Zumo32U4LCD lcd;
Zumo32U4Motors motors;
Zumo32U4LineSensors sensors;
//<---------------------------------------------->

//<--------------Variables---------------->
unsigned int sensor_values[5];
unsigned int line = 600;
//<-------------------------------------->

//<--------Functions declaration--------->
void showSensorsValues();

//<-------------------------------------->

//<-------------Class-------------------->
class LineSensors
{
  public:
  uint8_t sensor_number;

  void sensorSet(uint8_t number)
  {
    sensor_number = number;
    sensors.calibrate();
  }
  unsigned int lSensorScan()
  {
    sensors.read(sensor_values);
    return sensor_values[sensor_number];
  }
};
//--------------------------------------->

void setup() {
  sensors.calibrate();
  sensors.initFiveSensors();
}

void loop() {
  //<-------------Setup sensors----------->
   LineSensors leftsensor;
   LineSensors rightsensor;
   LineSensors mleftsensor;
   LineSensors middlesensor;
   LineSensors mrightsensor;

   leftsensor.sensorSet(0);
   mleftsensor.sensorSet(1);
   middlesensor.sensorSet(2);
   mrightsensor.sensorSet(3);
   rightsensor.sensorSet(4);
  //<------------------------------------>
    if(leftsensor.lSensorScan() < line && middlesensor.lSensorScan() > line && rightsensor.lSensorScan() < line)
    {
      motors.setSpeeds(100, 100);
      delay(200);
    }
   /* else if(mleftsensor.lSensorScan() > line && leftsensor.lSensorScan() < line)
    {
      motors.setSpeeds(-100, 100);
      delay(300);
    }
    else if(mrightsensor.lSensorScan() > line && rightsensor.lSensorScan() < line)
    {
      motors.setSpeeds(100, -100);
      delay(300);
    }*/
    else if(leftsensor.lSensorScan() > line)
    {
      motors.setSpeeds(-100, 100);
      delay(500);
    }
    else if(rightsensor.lSensorScan() > line)
    {
      motors.setSpeeds(100, -100);
      delay(500);
    }



   
   if(buttonA.getSingleDebouncedPress()) showSensorsValues(leftsensor,
                                                            rightsensor,
                                                            mleftsensor,
                                                            middlesensor,
                                                            mrightsensor);
   
}

//<--------Functions body------>
void showSensorsValues(LineSensors leftsensor,
    LineSensors rightsensor,
    LineSensors mleftsensor,
    LineSensors middlesensor,
    LineSensors mrightsensor)
{
  motors.setSpeeds(0, 0);
  uint8_t sensN=0;
  while(true)
  {
    switch(sensN)
    {
      case 0:
        lcd.clear();
        lcd.gotoXY(0, 0);
        lcd.print(F("L "));
        lcd.print(leftsensor.lSensorScan());
        lcd.gotoXY(0, 1);
        lcd.print(F("<A /B C>"));
        delay(1000);
      break;
      case 1:
        lcd.clear();
        lcd.gotoXY(0, 0);
        lcd.print(F("ML "));
        lcd.print(mleftsensor.lSensorScan());
        lcd.gotoXY(0, 1);
        lcd.print(F("<A /B C>"));
        delay(1000);
      break;
      case 2:
        lcd.clear();
        lcd.gotoXY(0, 0);
        lcd.print(F("M "));
        lcd.print(middlesensor.lSensorScan());
        lcd.gotoXY(0, 1);
        lcd.print(F("<A /B C>"));
        delay(1000);
      break;
      case 3:
        lcd.clear();
        lcd.gotoXY(0, 0);
        lcd.print(F("MR "));
        lcd.print(mrightsensor.lSensorScan());
        lcd.gotoXY(0, 1);
        lcd.print(F("<A /B C>"));
        delay(1000);
      break;
      case 4:
        lcd.clear();
        lcd.gotoXY(0, 0);
        lcd.print(F("R "));
        lcd.print(rightsensor.lSensorScan());
        lcd.gotoXY(0, 1);
        lcd.print(F("<A /B C>"));
        delay(1000);
      break;
      default:
        if(sensN<0) sensN=0;
        if(sensN>4) sensN=4;
      break;
    }
    if(buttonA.getSingleDebouncedPress()) sensN--;
    if(buttonB.getSingleDebouncedPress()) loop();
    if(buttonC.getSingleDebouncedPress()) sensN++;
  }

}

