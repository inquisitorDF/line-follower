#include <avr/pgmspace.h>
#include <Wire.h>
#include <Zumo32U4.h>
//<----------------Setup Zumo----------------->
Zumo32U4ButtonA buttonA;
Zumo32U4Motors motors;
Zumo32U4LineSensors sensors;
//<---------------------------------------------->

//<--------------Variables---------------->
unsigned int sensor_values[5];
unsigned int line = 600;
//<-------------------------------------->


void setup() {
  sensors.calibrate();
  sensors.initFiveSensors();
  buttonA.waitForButton();
}

void loop() {
    sensors.read(sensor_values);

    if(sensor_values[0] < line && sensor_values[2] > line && sensor_values[4] < line){
      motors.setSpeeds(400, 400);
    }
    else if(sensor_values[1] > line && sensor_values[0] < line){
      motors.setSpeeds(50, 200);
    }
    else if(sensor_values[3] > line && sensor_values[4] < line){
      motors.setSpeeds(200, 50);
    }
    else if(sensor_values[0] > line){
      motors.setSpeeds(0, 200);
    }
    else if(sensor_values[4] > line){
      motors.setSpeeds(200, 0);
    }
    else if(sensor_values[4] < line && sensor_values[3] < line && sensor_values[1] < line && sensor_values[2] < line && sensor_values[0] < line){
      motors.setSpeeds(100, 100);
    }
}


